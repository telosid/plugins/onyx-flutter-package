#import "OnyxPlugin.h"
#import <Flutter/Flutter.h>

@implementation OnyxPlugin

NSDictionary<NSNumber *, NSString *> *onyxConfigurationKeysToString = @{
    @(licenseKey): @"licenseKey",
    @(returnRawImage): @"returnRawImage",
    @(returnProcessedImage): @"returnProcessedImage",
    @(returnEnhancedImage): @"returnEnhancedImage",
    @(returnSlapImage): @"returnSlapImage",
    @(returnSlapWSQ): @"returnSlapWSQ",
    @(shouldBinarizeProcessedImage): @"shouldBinarizeProcessedImage",
    @(returnFullFrameImage): @"returnFullFrameImage",
    @(returnWSQ): @"returnWSQ",
    @(returnFingerprintTemplate): @"returnFingerprintTemplate",
    @(fullFrameMaxImageHeight): @"fullFrameMaxImageHeight",
    @(cropSizeHeight): @"cropSizeHeight",
    @(cropSizeWidth): @"cropSizeWidth",
    @(cropFactor): @"cropFactor",
    @(showLoadingSpinner): @"showLoadingSpinner",
    @(useManualCapture): @"useManualCapture",
    @(manualCaptureText): @"manualCaptureText",
    @(captureFingersText): @"captureFingersText",
    @(captureThumbText): @"captureThumbText",
    @(fingersNotInFocusText): @"fingersNotInFocusText",
    @(thumbNotInFocusText): @"thumbNotInFocusText",
    @(useOnyxLive): @"useOnyxLive",
    @(useFlash): @"useFlash",
    @(reticleOrientation): @"reticleOrientation",
    @(computeNfiqMetrics): @"computeNfiqMetrics",
    @(targetPixelsPerInch): @"targetPixelsPerInch",
    @(subjectId): @"subjectId",
    @(uploadMetrics): @"uploadMetrics",
    @(returnOnyxErrorOnLowQuality): @"returnOnyxErrorOnLowQuality",
    @(captureQualityThreshold): @"captureQualityThreshold",
    @(fingerDetectionTimeout): @"fingerDetectionTimeout"
};

static FlutterMethodChannel* channel;
Onyx* _configuredOnyx;
static FlutterViewController * _flutterController;

#pragma mark - flutter controller Property
+(FlutterViewController *)flutterViewController{
    return _flutterController;
}
+(void) setFlutterViewController:(FlutterViewController *) newFlutterViewController{
    NSLog(@"newFlutterViewController: %@", newFlutterViewController);
    _flutterController=newFlutterViewController;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    channel = [FlutterMethodChannel
                methodChannelWithName:@"com.dft.onyx_plugin/methodChannel"
                binaryMessenger:[registrar messenger]];
    OnyxPlugin* instance = [[OnyxPlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([@"configureOnyx" isEqualToString:call.method]) {
      [[self configureOnyx: call] buildOnyxConfiguration];
      result(0);
  } else {
      result(FlutterMethodNotImplemented);
  }
}

#pragma mark - Onyx Callbacks

-(void(^)(Onyx* configuredOnyx))onyxCallback {
    return ^(Onyx* configuredOnyx) {
        NSLog(@"Onyx Callback");
        NSLog(@"ConfiguredOnyx before: %@", configuredOnyx);
        dispatch_async(dispatch_get_main_queue(), ^{
            _configuredOnyx = configuredOnyx;
            NSLog(@"_configuredOnyx after: %@", _configuredOnyx);
            [channel invokeMethod:@"onyx_configured" arguments:nil];
            if(_configuredOnyx != nil && [[self topViewController] isMemberOfClass:[FlutterViewController class]]) {
                NSLog(@"_flutterController: %@", _flutterController);
                [_configuredOnyx capture:_flutterController];
            } else {
                NSLog(@"Unable to start ONYX.");
            }
        });
    };
}

-(void(^)(OnyxResult* onyxResult))onyxSuccessCallback {
    return ^(OnyxResult* onyxResult) {
        NSLog(@"Onyx Success Callback");
        NSMutableDictionary* flutterResults=  [self getOnyxResultFlutterParams:onyxResult];
        [_flutterController.navigationController popToRootViewControllerAnimated:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
                [channel invokeMethod:@"onyx_success" arguments:flutterResults];
        });
        [_flutterController.navigationController popToRootViewControllerAnimated:YES];
    };
}

/*
handles the onyx error callback.
*/
-(void(^)(OnyxError* onyxError)) onyxErrorCallback {
    return ^(OnyxError* onyxError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Onyx Error Callback");
            [channel invokeMethod:@"onyx_error"
                        arguments:@{@"errorMessage":  onyxError.errorMessage}
             ];
        });
    };
}


/*
 Configures onyx with the passed in parameters.
 */
-(OnyxConfigurationBuilder*) configureOnyx:(FlutterMethodCall*)call{
    OnyxConfigurationBuilder* onyxConfigBuilder = [[OnyxConfigurationBuilder alloc] init];
    onyxConfigBuilder
        .licenseKey(call.arguments[onyxConfigurationKeysToString[@(licenseKey)]])
        .successCallback([self onyxSuccessCallback])
        .errorCallback([self onyxErrorCallback])
        .onyxCallback([self onyxCallback])
        .returnRawImage([call.arguments[onyxConfigurationKeysToString[@(returnRawImage)]] boolValue])
        .returnProcessedImage([call.arguments[onyxConfigurationKeysToString[@(returnProcessedImage)]] boolValue])
        .returnEnhancedImage([call.arguments[onyxConfigurationKeysToString[@(returnEnhancedImage)]] boolValue])
        .returnSlapImage([call.arguments[onyxConfigurationKeysToString[@(returnSlapImage)]] boolValue])
        .returnSlapWsqData([call.arguments[onyxConfigurationKeysToString[@(returnSlapWSQ)]] boolValue])
        .shouldBinarizeProcessedImage([onyxConfigurationKeysToString[@(shouldBinarizeProcessedImage)] boolValue])
        .returnFullFrameImage([call.arguments[onyxConfigurationKeysToString[@(returnFullFrameImage)]] boolValue])
        .returnWSQ([call.arguments[onyxConfigurationKeysToString[@(returnWSQ)]] boolValue])
        .showLoadingSpinner([call.arguments[onyxConfigurationKeysToString[@(showLoadingSpinner)]] boolValue])
        .useManualCapture([call.arguments[onyxConfigurationKeysToString[@(useManualCapture)]] boolValue])
        .useOnyxLive([call.arguments[onyxConfigurationKeysToString[@(useOnyxLive)]] boolValue])
        .useFlash([call.arguments[onyxConfigurationKeysToString[@(useFlash)]] boolValue])
        .computeNfiqMetrics([call.arguments[onyxConfigurationKeysToString[@(computeNfiqMetrics)]] boolValue])
        .subjectId(call.arguments[onyxConfigurationKeysToString[@(subjectId)]])
        .uploadMetrics([call.arguments[onyxConfigurationKeysToString[@(uploadMetrics)]] boolValue])
        .returnOnyxErrorOnLowQuality([call.arguments[onyxConfigurationKeysToString[@(returnOnyxErrorOnLowQuality)]] boolValue])
        .captureQualityThreshold([call.arguments[onyxConfigurationKeysToString[@(captureQualityThreshold)]] floatValue])
        .fingerDetectionTimeout([call.arguments[onyxConfigurationKeysToString[@(fingerDetectionTimeout)]] intValue]);

    FingerprintTemplateType fingerprintTemplateType = NONE;
    if ([call.arguments[onyxConfigurationKeysToString[@(returnFingerprintTemplate)]] isEqualToString:@"FingerprintTemplateType.INNOVATRICS"]) {
        fingerprintTemplateType = INNOVATRICS;
    } else if ([call.arguments[onyxConfigurationKeysToString[@(returnFingerprintTemplate)]] isEqualToString:@"FingerprintTemplateType.ISO"]) {
        fingerprintTemplateType = ISO;
    }
    onyxConfigBuilder.returnFingerprintTemplate(fingerprintTemplateType);

    ReticleOrientation reticleOrientationForONYX = LEFT;
    if ([call.arguments[onyxConfigurationKeysToString[@(reticleOrientation)]] isEqualToString:@"RIGHT"]) {
        reticleOrientationForONYX = RIGHT;
    } else if([call.arguments[onyxConfigurationKeysToString[@(reticleOrientation)]] isEqualToString:@"THUMB_PORTRAIT"]) {
        reticleOrientationForONYX = THUMB_PORTRAIT;
    }
    onyxConfigBuilder.reticleOrientation(reticleOrientationForONYX);
    // Crop Factor
    if (![call.arguments[onyxConfigurationKeysToString[@(cropFactor)]] isEqualToString:@""]) {
        onyxConfigBuilder.cropFactor([call.arguments[onyxConfigurationKeysToString[@(cropFactor)]] floatValue]);
    }
    // Crop Size
    if (![call.arguments[onyxConfigurationKeysToString[@(cropSizeHeight)]] isEqualToString:@""]
        && ![call.arguments[onyxConfigurationKeysToString[@(cropSizeWidth)]] isEqualToString:@""]) {
        onyxConfigBuilder.cropSize(CGSizeMake([call.arguments[onyxConfigurationKeysToString[@(cropSizeWidth)]] floatValue], [call.arguments[onyxConfigurationKeysToString[@(cropSizeHeight)]] floatValue]));
    }
    // Full Frame Max Image Height
    if (![call.arguments[onyxConfigurationKeysToString[@(fullFrameMaxImageHeight)]] isEqualToString:@""]) {
        onyxConfigBuilder.fullFrameMaxImageHeight([call.arguments[onyxConfigurationKeysToString[@(fullFrameMaxImageHeight)]] floatValue]);
    }
    // Target Pixels Per Inch
    if (![call.arguments[onyxConfigurationKeysToString[@(targetPixelsPerInch)]] isEqualToString:@""]) {
        onyxConfigBuilder.targetPixelsPerInch([call.arguments[onyxConfigurationKeysToString[@(targetPixelsPerInch)]] floatValue]);
    }
    // Subject Id
    if (![call.arguments[onyxConfigurationKeysToString[@(subjectId)]] isEqualToString:@""]) {
        onyxConfigBuilder.subjectId(call.arguments[onyxConfigurationKeysToString[@(subjectId)]]);
    }
    // Manual Capture Text
    if (![call.arguments[onyxConfigurationKeysToString[@(manualCaptureText)]] isEqualToString:@""]) {
        onyxConfigBuilder.manualCaptureText(call.arguments[onyxConfigurationKeysToString[@(manualCaptureText)]]);
    }
    // Capture Fingers Text
    if (![call.arguments[onyxConfigurationKeysToString[@(captureFingersText)]] isEqualToString:@""]) {
        onyxConfigBuilder.captureFingersText(call.arguments[onyxConfigurationKeysToString[@(captureFingersText)]]);
    }
    // Capture Thumb Text
    if (![call.arguments[onyxConfigurationKeysToString[@(captureThumbText)]] isEqualToString:@""]) {
        onyxConfigBuilder.captureThumbText(call.arguments[onyxConfigurationKeysToString[@(captureThumbText)]]);
    }
    // Fingers Not In Focus Text
    if (![call.arguments[onyxConfigurationKeysToString[@(fingersNotInFocusText)]] isEqualToString:@""]) {
        onyxConfigBuilder.fingersNotInFocusText(call.arguments[onyxConfigurationKeysToString[@(fingersNotInFocusText)]]);
    }
    // Thumb Not In Focus Text
    if (![call.arguments[onyxConfigurationKeysToString[@(thumbNotInFocusText)]] isEqualToString:@""]) {
        onyxConfigBuilder.thumbNotInFocusText(call.arguments[onyxConfigurationKeysToString[@(thumbNotInFocusText)]]);
    }
    return onyxConfigBuilder;
}

///Gets the onyx results as a dictionary of params to pass to flutter.
-(NSMutableDictionary*) getOnyxResultFlutterParams:(OnyxResult*) onyxResult{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:8];
    //add images
    if ([onyxResult getRawFingerprintImages] != nil) {
        [dict setObject:[NSMutableArray arrayWithArray:[self getImageByteArrays: [onyxResult getRawFingerprintImages]]] forKey:@"rawFingerprintImages"];
    }
    if ([onyxResult getProcessedFingerprintImages] != nil) {
        [dict setObject:[NSMutableArray arrayWithArray:[self getImageByteArrays: [onyxResult getProcessedFingerprintImages]]] forKey:@"processedFingerprintImages"];
    }
    if ([onyxResult getEnhancedFingerprintImages] != nil) {
        [dict setObject:[NSMutableArray arrayWithArray:[self getImageByteArrays: [onyxResult getEnhancedFingerprintImages]]] forKey:@"enhancedFingerprintImages"];
    }
    if ([onyxResult getSlapImage] != nil) {
        [dict setObject:UIImagePNGRepresentation([onyxResult getSlapImage]) forKey:@"slapImage"];
    }
    if ([onyxResult getSlapWsqData] != nil) {
        [dict setObject:[onyxResult getSlapWsqData] forKey:@"slapWsqData"];
    }
    if ([onyxResult getFullFrameImage] != nil) {
        [dict setObject:UIImagePNGRepresentation([onyxResult getFullFrameImage]) forKey:@"fullFrameImage"];
    }
    if ([onyxResult getWsqData] != nil && [onyxResult getWsqData].count != 0) {
        [dict setObject:[NSMutableArray arrayWithArray:[onyxResult getWsqData]] forKey:@"wsqData"];
    }
    //add templates
    [dict setObject:[NSMutableArray arrayWithArray:[self getTemplateFlutterArrays: [onyxResult getFingerprintTemplates]]] forKey:@"iosFingerprintTemplates"];

    //add metrics
    CaptureMetrics* metrics= [onyxResult getMetrics];
    if(metrics != nil){
        [dict setObject:[NSNumber numberWithFloat:[metrics getLivenessConfidence]] forKey:@"livenessConfidence"];
        [dict setObject:[NSNumber numberWithFloat:[metrics getQualityMetric]] forKey:@"qualityMetric"];
        NSMutableArray* nfiqArray=[metrics getNfiqMetrics];
        NSUInteger nfiqCount = [nfiqArray count];
        NSMutableArray *nfiqScores = [[NSMutableArray alloc] initWithCapacity:nfiqCount];
        for(NfiqMetrics* nfiq in nfiqArray){
            if(nfiq != nil){
                [nfiqScores addObject: [NSNumber numberWithInt:[nfiq getNfiqScore]]];
            }
        }
        [dict setObject:[NSMutableArray arrayWithArray:nfiqScores] forKey:@"nfiqScores"];
    }
    return dict;
}

/*
 Converts an array of templates for flutter.
*/
-(NSMutableArray*) getTemplateFlutterArrays:(NSMutableArray*) templateData{
    if(!templateData){
        return [[NSMutableArray alloc] initWithCapacity:0];
    }
    NSUInteger count = [templateData count];
    NSMutableArray *returnArray = [[NSMutableArray alloc] initWithCapacity:count];
    for (NSData* data in templateData) {
        [returnArray addObject:  [data base64EncodedDataWithOptions:NSUTF8StringEncoding]];
    }
    return returnArray;
}

/*
 Converts an array of UIImages to an array of byte arrays.
*/
-(NSMutableArray*) getImageByteArrays:(NSMutableArray*) imageArray{
    if(!imageArray){
        return [[NSMutableArray alloc] initWithCapacity:0];
    }
    NSUInteger count = [imageArray count];
    NSMutableArray *returnArray = [[NSMutableArray alloc] initWithCapacity:count];
    for (UIImage* image in imageArray) {
        [returnArray addObject:  UIImagePNGRepresentation(image)];
    }
    return returnArray;
}

- (UIViewController *)topViewController{
   return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
 }

 - (UIViewController *)topViewController:(UIViewController *)rootViewController
 {
   if ([rootViewController isKindOfClass:[UINavigationController class]]) {
     UINavigationController *navigationController = (UINavigationController *)rootViewController;
     return [self topViewController:[navigationController.viewControllers lastObject]];
   }
   if ([rootViewController isKindOfClass:[UITabBarController class]]) {
     UITabBarController *tabController = (UITabBarController *)rootViewController;
     return [self topViewController:tabController.selectedViewController];
   }
   if (rootViewController.presentedViewController) {
     return [self topViewController:rootViewController];
   }
   return rootViewController;
 }

@end
