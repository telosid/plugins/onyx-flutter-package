 #import <Flutter/Flutter.h>
#import <UIKit/UIKit.h>
#import <OnyxCamera/Onyx.h>
#import <OnyxCamera/OnyxConfigurationBuilder.h>
#import <OnyxCamera/OnyxConfiguration.h>
#import <OnyxCamera/CaptureNetController.h>
#import <OnyxCamera/OnyxViewController.h>
#import <OnyxCamera/OnyxEnums.h>

@interface OnyxPlugin : NSObject<FlutterPlugin>


typedef NS_ENUM(NSInteger, OnyxConfig) {
    licenseKey,
    returnRawImage,
    returnProcessedImage,
    returnEnhancedImage,
    returnSlapImage,
    returnSlapWSQ,
    shouldBinarizeProcessedImage,
    returnFullFrameImage,
    fullFrameMaxImageHeight,
    returnWSQ,
    returnFingerprintTemplate,
    cropSizeWidth,
    cropSizeHeight,
    cropFactor,
    showLoadingSpinner,
    useManualCapture,
    manualCaptureText,
    captureFingersText,
    captureThumbText,
    fingersNotInFocusText,
    thumbNotInFocusText,
    useOnyxLive,
    useFlash,
    reticleOrientation,
    computeNfiqMetrics,
    targetPixelsPerInch,
    subjectId,
    uploadMetrics,
    returnOnyxErrorOnLowQuality,
    captureQualityThreshold,
    fingerDetectionTimeout
};

//@property FlutterMethodChannel* channel;
@property OnyxResult* onyxResult;
@property  Onyx* onyx;

+(FlutterViewController *)flutterViewController;
+(void) setFlutterViewController:(FlutterViewController *) newFlutterViewController;
-(void(^)(OnyxResult* onyxResult))onyxSuccessCallback;

-(void(^)(OnyxError* onyxError)) onyxErrorCallback;

-(void(^)(Onyx* configuredOnyx))onyxCallback;


@end
