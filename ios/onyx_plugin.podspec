#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint hello.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'onyx_plugin'
  s.version          = '8.4.1'
  s.summary          = 'ONYX plugin for Flutter.'
  s.description      = <<-DESC
A flutter plugin project for the ONYX SDK from Telos.
                       DESC
  s.homepage         = 'https://gitlab.com/telosid/plugins/onyx-flutter-package'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Telos Corporation' => 'help@telos.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'OnyxCamera', '8.4.1'
  s.platform = :ios, '11.0'
  #s.preserve_paths = 'Frameworks/*.framework'
  s.frameworks = 'OnyxCamera'
  #s.static_framework = true
  # onyx_plugin does not contain iphonesimulator slices for arm64 or armv7.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64 armv7' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64 armv7' }
end
