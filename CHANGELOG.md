## 0.0.2-dev.1

* initial release

## 0.0.2

* updated license

## 0.0.4

* fixed typo in license key spelling

## 1.0.0

* added use flash as an option

## 7.0.1-dev.1

* added iOS support.
* adjusted version number to match the onyx version.

## 7.0.1

* fixed issue with ios template

## 7.1.0

* updated ios to use onyx version 7.1.9
* updated android to use onyx version 7.1.1

## 7.1.1

* Fix various issues causing plugin and example project not to work

## 8.0.0

* Update for ONYX 8.x
* updated iOS to use OnyxCamera version 8.0.3
* updated Android to use onyx-camera version 8.0.3
* Added s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64 armv7' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64 armv7' } for Podfile

## 8.0.1

* Update to onyx-camera 8.3.0 for Android ONYX SDK
* Update to OnyxCamera 8.2.1 for iOS ONYX SDK
* Updated to use Android 33
* Updated README.md to better describe setup for example project

## 8.0.2

* Update with latest ONYX SDK options, including creation of a new OnyxConfig enum with all of the OnyxConfiguration options
* Fix handling of full frame image, WSQ, and slap image handling on iOS

## 8.1.0

* Update to version 8.3.6 of onyx-camera SDK that has proguard-rules.pro bundled as Android can now package up this file from libraries
* Update to use Android 34

## 8.1.1

* Add additional logging for FingerprintCaptureTask in onyx-camera SDK

## 8.2.0

* Bug fix for OnyxPlugin losing reference to variables due to `static` keyword usage