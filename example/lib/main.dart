import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:onyx_plugin/onyx.dart';

import 'settings_screen.dart';
import 'fingerprint_screen.dart';

Future<void> main() async {
  await dotenv.load();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late BuildContext appContext;

  @override
  void initState() {
    super.initState();
    OnyxCamera.state.addListener(() {
      if (OnyxCamera.state.isError) {
        var snackBar = SnackBar(
          content: Text(OnyxCamera.state.resultMessage),
        );
        ScaffoldMessenger.of(appContext).showSnackBar(snackBar);
      }
      if (OnyxCamera.state.status == OnyxStatuses.success) {
        Navigator.of(appContext)
            .push(MaterialPageRoute(builder: (context) => const FingerprintScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Onyx Flutter Demo',
        home: Builder(builder: (context) {
          appContext = context;
          if (OnyxCamera.state.status == OnyxStatuses.success) {
            return const FingerprintScreen();
          } else {
            return const SettingsScreen();
          }
        }));
  }
}
