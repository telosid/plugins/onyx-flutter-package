
package com.dft.onyx.plugin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import com.dft.onyxcamera.config.Onyx;
import com.dft.onyxcamera.config.OnyxConfiguration;
import com.dft.onyxcamera.config.OnyxConfigurationBuilder;
import com.dft.onyxcamera.config.OnyxError;
import com.dft.onyxcamera.config.OnyxResult;
import com.dft.onyxcamera.ui.reticles.Reticle;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * OnyxPlugin
 */
@SuppressLint("LogNotTimber")
public class OnyxCallbackHandler implements MethodCallHandler {
    public static final String ONYX_PLUGIN = "OnyxPlugin";
    public static Onyx configuredOnyx;
    public static Activity OnyxActivity;
    private OnyxPlugin onyxPlugin;

    public OnyxCallbackHandler(OnyxPlugin onyxPlugin) {
        super();
        this.onyxPlugin = onyxPlugin;
    }

    // The fingerprint template valid when setReturnFingerprintTemplate(true) on
    // config
    // private ArrayList<FingerprintTemplate> fingerprintTemplates;
    /// The function that's triggered by the onyx success event.
    private final OnyxConfiguration.SuccessCallback successCallback = new OnyxConfiguration.SuccessCallback() {
        @Override
        public void onSuccess(OnyxResult onyxResult) {
            Log.i(ONYX_PLUGIN, "onyx_plugin: native SuccessCallback reached");
            Map<String, Object> args = OnyxCallbackHelpers.getOnyxResultsMap(onyxResult);

            // Switch to main thread to send result to Flutter
            new Handler(Looper.getMainLooper()).post(() ->
                    onyxPlugin.getChannel().invokeMethod("onyx_success", args));

            if (OnyxActivity != null) {
                OnyxActivity.finish();
            }
        }
    };

    /* ONYX-RESULT */

    // The processed fingerprint Bitmap images indexed 0-3 for each finger , 0-3
    // print indices
    // private ArrayList<Bitmap> processedFingerprintImages;

    // Wave-Length Scalar Quantization for grayscale prints
    // The WSQ data as a byte array
    // private ArrayList<byte[]> wsqData;
    /// The function that's triggered by the onyx error event.
    private final OnyxConfiguration.ErrorCallback errorCallback = new OnyxConfiguration.ErrorCallback() {
        @Override
        public void onError(OnyxError onyxError) {
            Log.i(ONYX_PLUGIN, onyxError.getErrorMessage());
            Map<String, Object> args = new HashMap<>();
            args.put("errorMessage", onyxError.getErrorMessage());
            new Handler(Looper.getMainLooper()).post(() ->
                    onyxPlugin.getChannel().invokeMethod("onyx_error", args));

            if (OnyxActivity != null) {
                OnyxActivity.finish();
            }
        }
    };
    private final OnyxConfiguration.OnyxCallback onyxCallback = new OnyxConfiguration.OnyxCallback() {
        @Override
        public void onConfigured(Onyx onyx) {
            configuredOnyx = onyx;
            if (configuredOnyx != null) {
                new Handler(Looper.getMainLooper()).post(() -> {
                        onyxPlugin.getChannel().invokeMethod("onyx_configured", null);
                    Log.i(ONYX_PLUGIN, "onyx configured");
                    startOnyx();
                });
            }
        }
    };

    // #region The callback methods

    /// This should return a flutter callback result
    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        try {
            switch (call.method) {
                case "configureOnyx": {
                    configureOnyx(call);
                    result.success(null); // Respond immediately, callback will come later
                    break;
                }
                default: {
                    result.notImplemented();
                    break;
                }
            }
        } catch (Exception ex) {
            Log.i(ONYX_PLUGIN, "Exception: " + ex.getMessage(), ex);
            result.error("1", ex.getMessage(), ex);
        }
    }

    // #region The channel event methods.
    /// starts onyx
    private void startOnyx() {
        Log.i(ONYX_PLUGIN, "Onyx starting");
        onyxPlugin.getActivity().startActivityForResult(
                new Intent(onyxPlugin.getActivity(), OnyxActivity.class), 1337);
        Log.i(ONYX_PLUGIN, "Onyx started");
    }

    /// configures onyx.
    private void configureOnyx(@NonNull MethodCall call) {
        Log.i(ONYX_PLUGIN, "Onyx Configuration started");
        OnyxConfigurationBuilder onyxConfigurationBuilder = new OnyxConfigurationBuilder()
            .configActivity(onyxPlugin.getActivity()).licenseKey(call.argument(
                    OnyxConfig.LICENSE_KEY.getKey()))
            .successCallback(successCallback)
            .errorCallback(errorCallback)
            .onyxCallback(onyxCallback)
            .returnRawImage(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_RAW_IMAGE.getKey()))
            .returnProcessedImage(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_PROCESSED_IMAGE.getKey()))
            .returnEnhancedImage(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_ENHANCED_IMAGE.getKey()))
            .returnSlapImage(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_SLAP_IMAGE.getKey()))
            .returnSlapWsqData(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_SLAP_WSQ.getKey()))
            .shouldBinarizeProcessedImage(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.SHOULD_BINARIZE_PROCESSED_IMAGE.getKey()))
            .returnFullFrameImage(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_FULL_FRAME_IMAGE.getKey()))
            .returnWSQ(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_WSQ.getKey()))
            .cropFactor(OnyxCallbackHelpers.getFloatValue(call,
                    OnyxConfig.CROP_FACTOR.getKey()))
            .cropSize(OnyxCallbackHelpers.getDoubleValue(call,
                            OnyxConfig.CROP_SIZE_WIDTH.getKey()),
                    OnyxCallbackHelpers.getDoubleValue(call,
                            OnyxConfig.CROP_SIZE_HEIGHT.getKey()))
            .showLoadingSpinner(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.SHOW_LOADING_SPINNER.getKey()))
            .useManualCapture(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.USE_MANUAL_CAPTURE.getKey()))
            .useOnyxLive(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.USE_ONYX_LIVE.getKey()))
            .useFlash(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.USE_FLASH.getKey()))
            .computeNfiqMetrics(OnyxCallbackHelpers.getBoolValue(call,
                OnyxConfig.COMPUTE_NFIQ_METRICS.getKey()))
            .uploadMetrics(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.UPLOAD_METRICS.getKey()))
            .returnOnyxErrorOnLowQuality(OnyxCallbackHelpers.getBoolValue(call,
                    OnyxConfig.RETURN_ONYX_ERROR_ON_LOW_QUALITY.getKey()))
            .captureQualityThreshold(OnyxCallbackHelpers.getDoubleValue(call,
                    OnyxConfig.CAPTURE_QUALITY_THRESHOLD.getKey()))
            .fingerDetectionTimeout(OnyxCallbackHelpers.getIntValue(call,
                    OnyxConfig.FINGER_DETECTION_TIMEOUT.getKey()));

        String fingerprintTemplateTypeText = call.argument(
                OnyxConfig.RETURN_FINGERPRINT_TEMPLATE.getKey());
        OnyxConfiguration.FingerprintTemplateType fingerprintTemplateType =
                OnyxConfiguration.FingerprintTemplateType.NONE;
        if (OnyxConfiguration.FingerprintTemplateType.INNOVATRICS.toString()
                .equalsIgnoreCase(fingerprintTemplateTypeText)) {
            fingerprintTemplateType = OnyxConfiguration.FingerprintTemplateType.INNOVATRICS;
        } else if (OnyxConfiguration.FingerprintTemplateType.ISO.toString()
                .equalsIgnoreCase(fingerprintTemplateTypeText)) {
            fingerprintTemplateType = OnyxConfiguration.FingerprintTemplateType.ISO;
        }
        onyxConfigurationBuilder.returnFingerprintTemplate(fingerprintTemplateType);

        String reticleOrientationText = call.argument(
                OnyxConfig.RETICLE_ORIENTATION.getKey());
        Reticle.Orientation reticleOrientation = Reticle.Orientation.LEFT;
        if (Reticle.Orientation.RIGHT.toString().equalsIgnoreCase(reticleOrientationText)) {
            reticleOrientation = Reticle.Orientation.RIGHT;
        } else if (Reticle.Orientation.THUMB_PORTRAIT.toString().equalsIgnoreCase(reticleOrientationText)) {
            reticleOrientation = Reticle.Orientation.THUMB_PORTRAIT;
        }
        onyxConfigurationBuilder.reticleOrientation(reticleOrientation);

        try {
            onyxConfigurationBuilder.targetPixelsPerInch(OnyxCallbackHelpers.getDoubleValue(call,
                    OnyxConfig.TARGET_PIXELS_PER_INCH.getKey()));
        } catch (Exception ex) {
            // yes. we're assuming the error is due to the targetPPI being null.
            onyxConfigurationBuilder.targetPixelsPerInch(-1.0);
        }
        if (call.argument(OnyxConfig.FULL_FRAME_MAX_IMAGE_HEIGHT.getKey()) != null && call.argument(
                OnyxConfig.FULL_FRAME_MAX_IMAGE_HEIGHT.getKey()) != "") {
            try {
                onyxConfigurationBuilder.fullFrameMaxImageHeight(OnyxCallbackHelpers.getFloatValue(call,
                        OnyxConfig.FULL_FRAME_MAX_IMAGE_HEIGHT.getKey()));
            } catch (Exception e) {
                Log.e(ONYX_PLUGIN, "Exception getting " +
                        OnyxConfig.FULL_FRAME_MAX_IMAGE_HEIGHT);
            }
        }

        if (call.argument(OnyxConfig.SUBJECT_ID.getKey()) != null &&
                call.argument(OnyxConfig.SUBJECT_ID.getKey()) != "") {
            onyxConfigurationBuilder.subjectId(call.argument(OnyxConfig.SUBJECT_ID.getKey()));
        }

        if (call.argument(OnyxConfig.MANUAL_CAPTURE_TEXT.getKey()) != null &&
                call.argument(OnyxConfig.MANUAL_CAPTURE_TEXT.getKey()) != "") {
            onyxConfigurationBuilder.manualCaptureText(call.argument(
                    OnyxConfig.MANUAL_CAPTURE_TEXT.getKey()));
        }

        if (call.argument(OnyxConfig.CAPTURE_FINGERS_TEXT.getKey()) != null &&
                call.argument(OnyxConfig.CAPTURE_FINGERS_TEXT.getKey()) != "") {
            onyxConfigurationBuilder.captureFingersText(call.argument(
                    OnyxConfig.CAPTURE_FINGERS_TEXT.getKey()));
        }

        if (call.argument(OnyxConfig.CAPTURE_THUMB_TEXT.getKey()) != null &&
            call.argument(OnyxConfig.CAPTURE_THUMB_TEXT.getKey()) != "") {
            onyxConfigurationBuilder.captureThumbText(call.argument(
                    OnyxConfig.CAPTURE_THUMB_TEXT.getKey()));
        }

        if (call.argument(OnyxConfig.FINGERS_NOT_IN_FOCUS_TEXT.getKey()) != null &&
            call.argument(OnyxConfig.FINGERS_NOT_IN_FOCUS_TEXT.getKey()) != "") {
            onyxConfigurationBuilder.fingersNotInFocusText(call.argument(
                    OnyxConfig.FINGERS_NOT_IN_FOCUS_TEXT.getKey()));
        }

        if (call.argument(OnyxConfig.THUMB_NOT_IN_FOCUS_TEXT.getKey()) != null &&
            call.argument(OnyxConfig.THUMB_NOT_IN_FOCUS_TEXT.getKey()) != "") {
            onyxConfigurationBuilder.thumbNotInFocusText(call.argument(
                    OnyxConfig.THUMB_NOT_IN_FOCUS_TEXT.getKey()));
        }

        onyxConfigurationBuilder.buildOnyxConfiguration();
        Log.i(ONYX_PLUGIN, "Onyx Configuration Built");
    }
    // #endregion
}
