package com.dft.onyx.plugin;

import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;

import com.dft.onyx.FingerprintTemplate;
import com.dft.onyx.NfiqMetrics;
import com.dft.onyxcamera.config.OnyxResult;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;

import io.flutter.plugin.common.MethodCall;

/// the callback helper classes
public class OnyxCallbackHelpers {
    OnyxCallbackHelpers() {
        super();
    }

    /// Gets the bool value from a call parameter
    public static Boolean getBoolValue(@NonNull MethodCall call, String propertyName) {
        String boolValue = call.argument(propertyName);
        Log.i("OnyxPlugin", propertyName + " " + boolValue);
        return Boolean.parseBoolean(boolValue);
    }

    /// Gets the integer value from a call parameter
    public static Integer getIntValue(@NonNull MethodCall call, String propertyName) {
        String result = call.argument(propertyName);
        Log.i("OnyxPlugin", propertyName + " " + result);
        return Integer.valueOf(result);
    }

    /// Gets the double value from a call parameter
    public static Double getDoubleValue(@NonNull MethodCall call, String propertyName) {
        String result = call.argument(propertyName);
        Log.i("OnyxPlugin", propertyName + " " + result);
        return Double.valueOf(result);
    }

    /// Gets the float value from a call parameter
    public static Float getFloatValue(@NonNull MethodCall call, String propertyName) {
        String result = call.argument(propertyName);
        Log.i("OnyxPlugin", propertyName + " " + result);
        return Float.valueOf(result);
    }

    /// maps a list of bitmap values to a channel message's arguments.
    public static void addBitmapsArg(Map<String, Object> args, String argumentName, ArrayList<Bitmap> bitmaps) {
        if (bitmaps == null || bitmaps.isEmpty()) {
            return;
        }
        ArrayList<Object> argValue = new ArrayList<>();
        for (Bitmap bmImage : bitmaps) {
            if (bmImage != null) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bmImage.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                argValue.add(outputStream.toByteArray());
            }
        }
        if (!argValue.isEmpty()) {
            args.put(argumentName, argValue);
        }
    }

    /// populates a map with the onyx results.
    public static Map<String, Object> getOnyxResultsMap(OnyxResult onyxResult) {
        Map<String, Object> args = new HashMap<>();
        if (onyxResult.getRawFingerprintImages() != null) {
            OnyxCallbackHelpers.addBitmapsArg(args, "rawFingerprintImages",
                    onyxResult.getRawFingerprintImages());
        }
        if (onyxResult.getProcessedFingerprintImages() != null) {
            OnyxCallbackHelpers.addBitmapsArg(args, "processedFingerprintImages",
                    onyxResult.getProcessedFingerprintImages());

        }
        if (onyxResult.getEnhancedFingerprintImages() != null) {
            OnyxCallbackHelpers.addBitmapsArg(args, "enhancedFingerprintImages",
                    onyxResult.getEnhancedFingerprintImages());
        }
        args.put("slapWsqData", onyxResult.getSlapWsqData());
        args.put("wsqData", onyxResult.getWsqData());
        if(onyxResult.getSlapImage() != null){
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            onyxResult.getSlapImage().compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            args.put("slapImage", outputStream.toByteArray());
        }
        if(onyxResult.getFullFrameImage() != null){
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        onyxResult.getFullFrameImage().compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        args.put("fullFrameImage", outputStream.toByteArray());
        }
        if (onyxResult.getFingerprintTemplates() != null) {
            args.put("fingerprintTemplates", getTemplateMaps(onyxResult.getFingerprintTemplates()));
        }
         if (onyxResult.getMetrics() != null) {
             com.dft.onyxcamera.ui.CaptureMetrics metrics=onyxResult.getMetrics();
             args.put("livenessConfidence", metrics.getLivenessConfidence());
             args.put("qualityMetric", metrics.getQualityMetric());
             if (metrics.getNfiqMetrics() != null) {
                 ArrayList<Object> nfiqScores = new ArrayList<>();
                 List<NfiqMetrics> nfiqMetricsList = metrics.getNfiqMetrics();
                 for (int i = 0; i < nfiqMetricsList.size(); i++) {
                     if (nfiqMetricsList.get(i) != null) {
                         String nfiqScore = "";
                         nfiqScore = String.valueOf(nfiqMetricsList.get(i).getNfiqScore());
                         if (!nfiqScore.equals("")) {
                             nfiqScores.add(nfiqScore);
                         }

                     }
                 }
                 args.put("nfiqScores", nfiqScores);
             }
         }
        return args;
    }

    private static  ArrayList<Object>  getTemplateMaps(ArrayList<FingerprintTemplate> templates){
        ArrayList<Object> results = new ArrayList<>();
        for (FingerprintTemplate finger : templates) {
            if(!finger.isEmpty()) {
                Map<String, Object> fingerMap = new HashMap<>();
                fingerMap.put("image", finger.getData());
                fingerMap.put("nfiqScore", finger.getNfiqScore());
                fingerMap.put("quality", finger.getQuality());
                fingerMap.put("customId", finger.getCustomId());
                fingerMap.put("location", finger.getFingerLocation().toString());
                results.add(fingerMap);
            }
        }
        return results;
    }
}
