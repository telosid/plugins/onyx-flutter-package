package com.dft.onyx.plugin;

public enum OnyxConfig {
    LICENSE_KEY("licenseKey"),
    RETURN_RAW_IMAGE("returnRawImage"),
    RETURN_PROCESSED_IMAGE("returnProcessedImage"),
    RETURN_ENHANCED_IMAGE("returnEnhancedImage"),
    RETURN_SLAP_IMAGE("returnSlapImage"),
    RETURN_SLAP_WSQ("returnSlapWSQ"),
    SHOULD_BINARIZE_PROCESSED_IMAGE("shouldBinarizeProcessedImage"),
    RETURN_FULL_FRAME_IMAGE("returnFullFrameImage"),
    FULL_FRAME_MAX_IMAGE_HEIGHT("fullFrameMaxImageHeight"),
    RETURN_WSQ("returnWSQ"),
    RETURN_FINGERPRINT_TEMPLATE("returnFingerprintTemplate"),
    CROP_SIZE("cropSize"),
    CROP_SIZE_WIDTH("cropSizeWidth"),
    CROP_SIZE_HEIGHT("cropSizeHeight"),
    CROP_FACTOR("cropFactor"),
    SHOW_LOADING_SPINNER("showLoadingSpinner"),
    USE_MANUAL_CAPTURE("useManualCapture"),
    MANUAL_CAPTURE_TEXT("manualCaptureText"),
    CAPTURE_FINGERS_TEXT("captureFingersText"),
    CAPTURE_THUMB_TEXT("captureThumbText"),
    FINGERS_NOT_IN_FOCUS_TEXT("fingersNotInFocusText"),
    THUMB_NOT_IN_FOCUS_TEXT("thumbNotInFocusText"),
    USE_ONYX_LIVE("useOnyxLive"),
    USE_FLASH("useFlash"),
    RETICLE_ORIENTATION("reticleOrientation"),
    COMPUTE_NFIQ_METRICS("computeNfiqMetrics"),
    TARGET_PIXELS_PER_INCH("targetPixelsPerInch"),
    SUBJECT_ID("subjectId"),
    UPLOAD_METRICS("uploadMetrics"),
    RETURN_ONYX_ERROR_ON_LOW_QUALITY("returnOnyxErrorOnLowQuality"),
    CAPTURE_QUALITY_THRESHOLD("captureQualityThreshold"),
    FINGER_DETECTION_TIMEOUT("fingerDetectionTimeout");

    private final String key;

    OnyxConfig(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }
}
