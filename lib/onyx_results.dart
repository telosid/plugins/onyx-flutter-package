part of onyx;

///the onyx results class.
class OnyxResults {
  static const RAW_FINGERPRINT_IMAGES = "rawFingerprintImages";
  static const PROCESSED_FINGERPRINT_IMAGES = "processedFingerprintImages";
  static const ENHANCED_FINGERPRINT_IMAGES = "enhancedFingerprintImages";
  static const WSQ_DATA = "wsqData";
  static const SLAP_WSQ_DATA = "slapWsqData";
  static const FINGERPRINT_TEMPLATES = "fingerprintTemplates";
  static const SLAP_IMAGE = "slapImage";
  static const FULL_FRAME_IMAGE = "fullFrameImage";
  static const LIVENESS_CONFIDENCE = "livenessConfidence";
  static const NFIQ_SCORES = "nfiqScores";
  static const HAS_MATCHES = "hasMatches";
  static const QUALITY_METRIC = "qualityMetric";

  /// The raw fingerprint images.
  List<Uint8List> rawFingerprintImages = [];

  /// The processed fingerprint images.
  List<Uint8List> processedFingerprintImages = [];

  /// The enhanced fingerprint Images.
  List<Uint8List> enhancedFingerprintImages = [];

  ///wsq Data
  List<Uint8List> wsqData = [];

  ///the fingerprint templates.
  List<OnyxFingerprintTemplate> fingerprintTemplates = [];

  ///the slap image.
  Uint8List? slapImage;

  ///the full frame image.
  Uint8List? fullFrameImage;

  /// The liveness confidence.
  double? livenessConfidence;

  ///the quality metric.
  double? qualityMetric;

  ///the nfiq scores.
  List<int> nfiqScores = [];

  /// if the fingerprints have a match.  null if not checked.
  /// Only returned in Android.
  bool? hasMatches;

  OnyxResults._([this.livenessConfidence = 0]);
  OnyxResults._loadResults(MethodCall call, [this.livenessConfidence = 0]) {
    if (call.arguments[RAW_FINGERPRINT_IMAGES] != null) {
      for (var image in call.arguments[RAW_FINGERPRINT_IMAGES]) {
        rawFingerprintImages.add(image);
      }
    }
    if (call.arguments[PROCESSED_FINGERPRINT_IMAGES] != null) {
      for (var image in call.arguments[PROCESSED_FINGERPRINT_IMAGES]) {
        processedFingerprintImages.add(image);
      }
    }
    if (call.arguments[ENHANCED_FINGERPRINT_IMAGES] != null) {
      for (var image in call.arguments[ENHANCED_FINGERPRINT_IMAGES]) {
        enhancedFingerprintImages.add(image);
      }
    }
    if (call.arguments[WSQ_DATA] != null) {
      for (var image in call.arguments[WSQ_DATA]) {
        wsqData.add(image);
      }
    }
    if (call.arguments[FINGERPRINT_TEMPLATES] != null) {
      for (var template in call.arguments[FINGERPRINT_TEMPLATES]) {
        fingerprintTemplates.add(OnyxFingerprintTemplate._(template));
      }
    }
    slapImage = call.arguments[SLAP_IMAGE];
    fullFrameImage = call.arguments[FULL_FRAME_IMAGE];
    livenessConfidence = call.arguments[LIVENESS_CONFIDENCE];
    if (call.arguments[NFIQ_SCORES] != null) {
      for (var score in call.arguments[NFIQ_SCORES]) {
        if (score != 0) {
          nfiqScores.add(score);
        }
      }
    }

    hasMatches = call.arguments[HAS_MATCHES];
    qualityMetric = call.arguments[QUALITY_METRIC];
  }
}
