// ignore_for_file: slash_for_doc_comments

part of 'onyx.dart';

/**
 * ONYX Configuration options for the OnyxConfigurationBuilder
 */
class OnyxOptions {
  /**
   * Specify the ONYX license key.
   * @param licenseKey (required) the Onyx license key
   */
  String? licenseKey = "";

  /**
   * Specify whether or not to return the raw image.
   * Returned in the OnyxResult
   */
  bool returnRawImage = false;

  /**
   * Specify whether or not to return the processed image.  This would be the most
   * common image type to use for conversion to WSQ or for matching.
   * Returned in the OnyxResult
   */
  bool returnProcessedImage = true;

  /**
   * Specify whether or not to return the enhanced image.  This is a special image that
   * works better with certain matching algorithms, but needs special testing to make sure it
   * will match.
   * Returned in the OnyxResult
   */
  bool returnEnhancedImage = false;

  /**
   * Specify whether or not to return a slap image. This contains all fingers captured
   * within a single image as specified in the FBI EBTS format.
   * Returned in the {@link OnyxResult}
   */
  bool returnSlapImage = false;

  /**
   * Specify whether or not to return a slap WSQ. This contains all fingers captured
   * within a single WSQ as specified in the FBI EBTS
   */
  bool returnSlapWSQ = false;

  /**
   * Specify whether or not to return the binarized processed fingerprint image
   * in the OnyxResult
   */
  bool shouldBinarizeProcessedImage = false;

  /**
   * Specify whether to return a FullFrame image.  This returns
   * an image that is suitable for finger detection in a full frame.
   * Returned in the OnyxResult
   */
  bool returnFullFrameImage = false;

  /**
   * Specify the maximum height for the FullFrame image to be returned, so for example,
   * if you want a 1920 height image returned, pass in 1920.0f for the value.  It will a full frame
   * image resized to 1920 for the maximum height.  To get the original height of the full frame, to
   * get full resolution, pass in a value of 1.0f.
   */
  double fullFrameMaxImageHeight = 1920.0;

  /**
   * Specify whether or not to return the WSQ image.  This will tell the SDK to return
   * a WSQ image suitable for use in matching and other applications.
   */
  bool returnWSQ = false;

  /**
   * Specify whether or not the capture task will return the specified
   * com.dft.onyxcamera.config.OnyxConfiguration.FingerprintTemplateType
   * in the OnyxResult
   * The ISO template is the ISO/IEC 19794-4 standard minutiae based template
   * The INNOVATRICS template is a proprietary fingerprint template that is highly scale tolerant and
   * useful for touchless matching
   */
  FingerprintTemplateType returnFingerprintTemplate = FingerprintTemplateType.NONE;

  /**
   * Specify the crop width for the capture image.  This will determine the image dimensions
   * in length and width for the resulting images.  This should be set to whatever is required
   * for the matching algorithm that is going to be used.
   */
  double cropSizeWidth = 300;

  /**
   * Specify the crop height for the capture image.  This will determine the image dimensions
   * in length and width for the resulting images.  This should be set to whatever is required
   * for the matching algorithm that is going to be used.
   */
  double cropSizeHeight = 512;

  /**
   * Specify the crop factor for the capture image.  This has a built in scaling factor and
   * this crop factor should be adjusted until you get an image that looks close to images that are
   * known to match against whatever biometric system you are attempting to match against.
   */
  double cropFactor = 1.0;

  /**
   * Specify that the spinner should be shown during OnyxConfiguration and during
   * image processing.
   */
  bool showLoadingSpinner = false;

  /**
   * Specify the method of capture to be a manual capture of the fingerprint.
   */
  bool useManualCapture = false;

  /**
   * Specify the manual capture text that is displayed on screen.
   */
  String manualCaptureText = "";

  /**
  * Specify the name of the text that is displayed on screen to indicate to the user
  * that they should hold their fingers steady, or keep them still during the capture.
  * You can also use the Translations Editor to override the translations for the strings.
  * See the Onyx Android Demo project for more information.
  */
  String captureFingersText = "";

  /**
  * Specify the name of the text that is displayed on screen to indicate to the user
  * that they should hold their thumb steady, or keep it still during the capture.
  * You can also use the Translations Editor to override the translations for the strings.
  * See the Onyx Android Demo project for more information.
  */
  String captureThumbText = "";

  /**
  * Specify the name of the text that is displayed on screen to indicate to the user
  * that they should move their fingers until they are in focus.
  * You can also use the Translations Editor to override the translations for the strings.
  * See the Onyx Android Demo project for more information.
  */
  String fingersNotInFocusText = "";

  /**
   * Specify the name of the text that is displayed on screen to indicate to the user
   * that they should move their thumb until it is in focus.
   * You can also use the Translations Editor to override the translations for the strings.
   * See the Onyx Android Demo project for more information.
   */
  String thumbNotInFocusText = "";

  /**
   * Specify to use ONYX LIVE_FINGER liveness detection as part of the configuration.
   * This will take additional processing time.
   */
  bool useOnyxLive = false;

  /**
   * Specify to use the flash
   */
  bool useFlash = true;

  /**
   * Specify the orientation of the reticle Reticle.Orientation
   */
  ReticleOrientation reticleOrientation = ReticleOrientation.LEFT;

  /**
   * Specify to compute NFIQ metrics
   */
  bool computeNfiqMetrics = false;

  /**
   * Specify target pixels per inch
   */
  double targetPixelsPerInch = -1.0;

  /**
   * Specify the subject id
   */
  String? subjectId;

  /**
   * For future use.  Not currently implemented.  Specify whether to upload capture metrics result.
   * This will be used to improve ONYX capture and quality performance in the future.
   */
  bool uploadMetrics = true;

  /**
   * Specify to return OnyxError on low quality imagery
   */
  bool returnOnyxErrorOnLowQuality = true;

  /**
   * Specify a double value between 0.0 - 1.0 to set the threshold for QualityNet to capture.  Higher
   * values mean higher image quality.
   */
  double captureQualityThreshold = 0.8;

  /**
   * Finger detection timeout in seconds (default is 60 seconds). This configuration option
   * describes how long to wait before stopping the detection state machine. After waiting the
   * specified time, the detection state machine will throw a NO_FINGERS_DETECTED OnyxError to the
   * caller.
   */
  int fingerDetectionTimeout = 60;

  ///returns the options as a serialized list.
  dynamic toParams() {
    return <String, dynamic>{
      OnyxConfig.licenseKey.toValueString(): licenseKey,
      OnyxConfig.returnRawImage.toValueString(): returnRawImage.toString(),
      OnyxConfig.returnProcessedImage.toValueString(): returnProcessedImage.toString(),
      OnyxConfig.returnEnhancedImage.toValueString(): returnEnhancedImage.toString(),
      OnyxConfig.returnSlapImage.toValueString(): returnSlapImage.toString(),
      OnyxConfig.returnSlapWSQ.toValueString(): returnSlapWSQ.toString(),
      OnyxConfig.shouldBinarizeProcessedImage.toValueString(): shouldBinarizeProcessedImage.toString(),
      OnyxConfig.returnFullFrameImage.toValueString(): returnFullFrameImage.toString(),
      OnyxConfig.fullFrameMaxImageHeight.toValueString(): (fullFrameMaxImageHeight).toString(),
      OnyxConfig.returnWSQ.toValueString(): returnWSQ.toString(),
      OnyxConfig.returnFingerprintTemplate.toValueString(): returnFingerprintTemplate.toString(),
      OnyxConfig.cropSizeWidth.toValueString(): cropSizeWidth.toString(),
      OnyxConfig.cropSizeHeight.toValueString(): cropSizeHeight.toString(),
      OnyxConfig.cropFactor.toValueString(): cropFactor.toString(),
      OnyxConfig.showLoadingSpinner.toValueString(): showLoadingSpinner.toString(),
      OnyxConfig.useManualCapture.toValueString(): useManualCapture.toString(),
      OnyxConfig.manualCaptureText.toValueString(): manualCaptureText.toString(),
      OnyxConfig.captureFingersText.toValueString(): captureFingersText.toString(),
      OnyxConfig.captureThumbText.toValueString(): captureThumbText.toString(),
      OnyxConfig.fingersNotInFocusText.toValueString(): fingersNotInFocusText.toString(),
      OnyxConfig.thumbNotInFocusText.toValueString(): thumbNotInFocusText.toString(),
      OnyxConfig.useOnyxLive.toValueString(): useOnyxLive.toString(),
      OnyxConfig.useFlash.toValueString(): useFlash.toString(),
      OnyxConfig.reticleOrientation.toValueString(): reticleOrientation.toValueString(),
      OnyxConfig.computeNfiqMetrics.toValueString(): computeNfiqMetrics.toString(),
      OnyxConfig.targetPixelsPerInch.toValueString(): (targetPixelsPerInch).toString(),
      OnyxConfig.subjectId.toValueString(): (subjectId ?? ""),
      OnyxConfig.uploadMetrics.toValueString(): uploadMetrics.toString(),
      OnyxConfig.returnOnyxErrorOnLowQuality.toValueString(): returnOnyxErrorOnLowQuality.toString(),
      OnyxConfig.captureQualityThreshold.toValueString(): captureQualityThreshold.toString(),
      OnyxConfig.fingerDetectionTimeout.toValueString(): fingerDetectionTimeout.toString(),
    };
  }
}
