///the onyx fingerprint plugin.
library onyx;

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

part "onyx_camera.dart";
part "onyx_options.dart";
part "onyx_results.dart";
part "onyx_state.dart";
part "onyx_statuses.dart";
part "onyx_fingerprint_template.dart";

enum ReticleOrientation { LEFT, RIGHT, THUMB_PORTRAIT }

extension ReticleOrientationExtension on ReticleOrientation {
  String toValueString() {
    return this.toString().split('.').last;
  }
}

enum FingerprintTemplateType { NONE, INNOVATRICS, ISO }

extension FingerprintTemplateTypeExtension on FingerprintTemplateType {
  String toValueString() {
    return this.toString().split('.').last;
  }
}

enum OnyxConfig {
  licenseKey,
  returnRawImage,
  returnProcessedImage,
  returnEnhancedImage,
  returnSlapImage,
  returnSlapWSQ,
  shouldBinarizeProcessedImage,
  returnFullFrameImage,
  fullFrameMaxImageHeight,
  returnWSQ,
  returnFingerprintTemplate,
  cropSizeWidth,
  cropSizeHeight,
  cropFactor,
  showLoadingSpinner,
  useManualCapture,
  manualCaptureText,
  captureFingersText,
  captureThumbText,
  fingersNotInFocusText,
  thumbNotInFocusText,
  useOnyxLive,
  useFlash,
  reticleOrientation,
  computeNfiqMetrics,
  targetPixelsPerInch,
  subjectId,
  uploadMetrics,
  returnOnyxErrorOnLowQuality,
  captureQualityThreshold,
  fingerDetectionTimeout
}

extension OnyxConfigExtension on OnyxConfig {
  String toValueString() {
    return this.toString().split('.').last;
  }
}
