# Onyx Flutter Package

This is not free software. This plugin uses the Onyx software development kits (SDKs) for Android and iOS. It requires a license agreement with:[Diamond Fortress Technologies, Inc.](https://diamondfortress.com/)

--------

## Package Requirements

### Example Project

Before running the example project, check that your local.properties contains flutter.sdk path and sdk.dir are pointed to the correct locations for flutter and android sdk respectively.
```Bash
sdk.dir=/Users/davidbuggay/Library/Android/sdk
flutter.sdk=/Users/davidbuggay/Workspace/flutter
onyx_plugin.dir=../../android
```
The example also uses a `.env` file to hold the license key, instead of hard coding it.
You can obtain a license key from Telos if you do not have one.

If this is your first time running the flutter project you may have to do a ```flutter pub get```

To run the example project, run `Example/lib/main.dart`.  The sample app load to a screen that shows all of the onyx camera settings, and provides the ability to start the onyx camera with any of the settings on the initial screen.

### Onyx controls

The `OnyxCamera` class's options contains all of onyx camera options, including the license key.  please see the [documentation](https://pub.dev/documentation/onyx_plugin/latest/onyx/OnyxOptions-class.html) for a complete list of the configuration options.

Once the settings are configured, simply call `OnyxCamera.configureOnyx` to set the onyx configurations

Listen for changes to the OnyxCamera state to determine when the onyx package is configured (`state=configured`), is returning results(`state=success`), or has an error (`state=error`).
It is possible to configure and start Onyx separately, but this example project will start ONYX as soon as ONYX is ocnfigured.

##### Here's a bare bones implementation for getting started

>```dart
>OnyxCamera.state.addListener(() {
>    if (OnyxCamera.state.status == OnyxStatuses.configured) {
>        //starts onyx after it's configured
>        OnyxCamera.startOnyx();
>    }
>    if (OnyxCamera.state.isError) {
>        //handle any onyx related errors.  error messages are stored in the OnyxCamera.state.resultMessage variable.
>    }
>    if (OnyxCamera.state.status == OnyxStatuses.success) {
>        //do something when the onyx camera sends back results.  OnyxCamera.Results holds the onyx results.
>    }
>});
>//sets the license key
>OnyxCamera.options.licenseKey = "xxxx";
>//starts the onyx camera.
>OnyxCamera.configureOnyx();

--------

## Android setup

The minimum android version supported is `24`.  More details for installing the plugin can be found at [pub.dev](https://pub.dev/packages/onyx_plugin/install).

Add the Onyx native package's repository and dependency references below to your app's `android/app/build.gradle` file.

```Groovy
android {
    lintOptions {
        disable 'InvalidPackage'
    }
}
repositories {
  maven {
    url 'https://gitlab.com/api/v4/projects/29462567/packages/maven'
  }
}
dependencies {
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'com.google.android.gms:play-services-base:17.6.0'
    implementation 'com.dft.android:onyx-camera:8.3.6'
}
```

The plugin also needs permission to write to external storage, so in the app's `AndroidManifest.xml`, make sure to add the line below.

>```xml
><manifest xmlns:android="http://schemas.android.com/apk/res/android">
><uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

--------

## iOS setup

### Onyx must Run on a device, and **will NOT run on an emulator!**

### Add the OnyxCamera specs repo to your CocoaPods installation
```bash
pod repo add gitlab-telosid-plugins https://gitlab.com/telosid/plugins/specs.git
pod install --repo-udpate
```

### Add the Gitlab source specs repository to your PodFile
- [ ] Insure that the `./iOS/PodFile` specifies the following sources:
- [ ] source 'https://gitlab.com/telosid/plugins/specs.git'
- [ ] source 'https://cdn.cocoapods.org'

### Set the iOS version to 11.
- [ ] Insure that the `./iOS/PodFile` specifies ios version 11 or later.

```
# Uncomment the next line to define a global platform for your project
platform :ios, '11.0'
```

--------

#### Adjust iOS project properties from `XCode`

- [ ]  Disable Bitcode
  >
  >- Select the root directory and go to `Build Settings`
  >- Search for `bitcode`
  >- Set `Enable Bitcode` to `No`
--------
  
#### Adjust Other Linker Flags
In the Other Linker Flags within the build settings of your Xcode project, add the following flags
```
-Xlinker
-ld_classic
```

#### Adjust the app permissions

- [ ] Open the project's `Info.plist` file, and paste the following lines at the bottom of the `<dict>` element.

    ```XML
   <key>NSCameraUsageDescription</key>
   <string>Capture fingerprint image</string>
  <key>NSPhotoLibraryAddUsageDescription</key>
  <string>Save Onyx Image Results</string>
  <key>NSPhotoLibraryUsageDescription</key>
  <string>Save Pictures</string>

--------

#### Add the navigation controller to the iOS/Runner/AppDelegate file

```objective-c
#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"
#import "OnyxPlugin.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GeneratedPluginRegistrant registerWithRegistry:self];
    //New Code
    FlutterViewController *flutterViewController =(FlutterViewController*)self.window.rootViewController;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:flutterViewController];
    [navigationController setNavigationBarHidden:YES];    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    OnyxPlugin.flutterViewController=flutterViewController;
    
  // Override point for customization after application launch.
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}
@end
```

## Publishing the onyx_plugin 

Be sure to increment versions in the pubspec.yaml file, the `ios/onyx_plugin.podspec`, 
and the iOS and Android ONYX SDK versions if necessary

First perform a dry run of the publish

```shell
flutter pub publish --dry-run
```

If the dry run succeeds, then you can publish the plugin

```shell
flutter pub publish
```